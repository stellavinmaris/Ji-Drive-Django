"""car_Hire_Django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from car_Hire_Django import settings
from drf_users.views_rest import UserViewSet, UserRegisterViewSet
from vehicle.views_rest import VehicleViewSet, VehicleCategoryViewSet
from rest_framework.authtoken import views
from django.conf.urls.static import static

router = routers.DefaultRouter()

router.register(r'auth/user', UserViewSet, base_name='auth-user')
router.register(r'register', UserRegisterViewSet, base_name='auth-register')
router.register(r'category', VehicleCategoryViewSet, base_name='category')
router.register(r'vehicle', VehicleViewSet, base_name='vehicle')

urlpatterns = [

    url('^', include('django.contrib.auth.urls')),

    url(r'^api/v1/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'api-token-auth/', views.obtain_auth_token),
    url(r'^admin/', admin.site.urls),

]

# if settings.DEBUG:
#     urlpatterns += [url(r'^media/profile_images/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),]

