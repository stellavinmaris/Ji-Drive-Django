from django.contrib import admin

# Register your models here.
from ordered_model.admin import OrderedModelAdmin

from vehicle.models import Vehicle, Category


# class VehicleAdmin(OrderedModelAdmin):
#     list_display = ('make', 'rate_per_hour', 'model',
#                     'body_type', 'color', 'registration_number', 'passanger_capacity','engine_capacity',
#                     'created_on', 'created_by')
#     list_filter = (
#         ('category', admin.RelatedOnlyFieldListFilter),
#     )


admin.site.register(Vehicle)
admin.site.register(Category)

