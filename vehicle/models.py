
from django.db import models, connection
from django.db.models.deletion import SET_NULL
from ordered_model.models import OrderedModel
from django.db.models import Count

class CategoryManager(models.Model):

    def get_queryset(self, *args, **kwargs):
        qs = super(CategoryManager, self).get_queryset()
        return qs

    def get_active(self, *args, **kwargs):
        qs = super(CategoryManager, self).get_queryset()
        qs.filter(deleted=False)
        return qs


class Category(models.Model):

    objects = CategoryManager()

    class Meta:
        ordering = ["title"]
        verbose_name_plural = "Categories"

    title = models.CharField(max_length=255, null=False, blank=False, unique=True)
    description = models.TextField(null=False, blank=True)
    deleted = models.BooleanField(default=False)
    image = models.FileField(null=True, blank=True)

    def __str__(self):
        return u"%s" % self.title

class VehicleManager(models.Model):

    def get_queryset(self, *args, **kwargs):
        qs = super(VehicleManager, self).get_queryset()
        return qs

    def get_active(self, *args, **kwargs):
        qs = super(VehicleManager, self).get_queryset()
        qs = qs.filter(deleted=False, state='PUB')

        return qs


    def get_publishedstats(self, *args, **kwargs):
        truncate_date = connection.ops.date_trunc_sql('day', 'created_on')
        qs = super(VehicleManager, self).get_queryset()
        qs = qs.filter(deleted=False, state='PUB')
        published_les = qs.count()
        qs = qs.extra({'date': truncate_date})
        qs = qs.values('date').annotate(published_les = Count('id', distinct=True)).order_by('date')
        #return {"total_les": published_les , "stats": qs}
        return  qs


class Vehicle(models.Model):

    objects = VehicleManager()

    category = models.ForeignKey('vehicle.Category', null=False, blank=False)
    authors = models.ManyToManyField('drf_users.User')
    make = models.CharField(max_length=255, null=False, blank=False)
    rate_per_hour = models.CharField(max_length=255, null=False, blank=False)
    model = models.CharField(max_length=255, null=False, blank=False)
    registration_number = models.CharField(max_length=255, null=False, blank=False)
    engine_capacity = models.CharField(max_length=255, null=False, blank=False)
    passanger_capacity = models.CharField(max_length=255, null=False, blank=False)
    image = models.FileField( null=True, blank=True)
    body_type = models.CharField(max_length=255, null=False, blank=False)
    insurance_certificate = models.FileField( null=True, blank=True)
    deleted = models.BooleanField(default=False)
    color = models.CharField(null=False, blank=False, default='#014386', max_length=50)


    order_with_respect_to = 'category'

    def __str__(self):
        return u"%s Vehicle" % self.model


    def save(self, *args, **kwargs):
        to_return = super(Vehicle, self).save(*args, **kwargs)
        obj = Vehicle.objects.get(id=self.id)
        return to_return

