import uuid
from uuid import UUID

import datetime
import django_filters
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.decorators import list_route, detail_route
from rest_framework.exceptions import MethodNotAllowed, ParseError, NotFound
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response


from vehicle.models import Vehicle, Category
from vehicle.serializers import VehicleSerializer, VehicleCategorySerializer



class VehicleFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Vehicle
        fields = ['category', 'make', 'deleted', 'authors', 'model']


class VehicleViewSet(viewsets.ModelViewSet):
    """
    __list__:

    __update__:

    __partial_update__:

    __reorder__ [PUT]:

    A batch reorder method, simply send an array of objects with `{"id":"","order":""}`

    """

    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer
    permission_classes = (IsAuthenticated,)
    search_fields = ('model',)
    filter_backends = (OrderingFilter, django_filters.rest_framework.DjangoFilterBackend, SearchFilter)
    filter_class = VehicleFilter
    ordering_fields = ('created_by', 'model', 'created_on', 'make', )

    def retrieve(self, request, *args, **kwargs):
        self.permission_classes = (IsAuthenticated,)
        return super(VehicleViewSet, self).retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        self.serializer_class = VehicleSerializer
        obj = self.get_object()
        if 'order' in request.data:
            if request.data['order'] != obj.order:
                obj.to(request.data['order'])

        return super(VehicleViewSet, self).update(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        self.serializer_class = VehicleSerializer
        obj = self.get_object()
        if 'order' in request.data:
            if request.data['order'] != obj.order:
                obj.to(request.data['order'])

        return super(VehicleViewSet, self).partial_update(request, *args, **kwargs)

    @list_route(methods=['put'])
    def reorder(self, request, *args, **kwargs):
        # Only staff/superuser should be able to do this one.
        if request.user.is_superuser or request.user.is_staff:
            id_list = []
            list = request.data
            for item in list:
                id_list.append(item['id'])
                obj = self.get_queryset().get(id=item['id'])
                obj.to(item['order'])
                qs = self.get_queryset().filter(id__in=id_list)

            return Response(self.get_serializer(qs, many=True).data)

        raise MethodNotAllowed('PUT')

    def create(self, request, *args, **kwargs):
        self.serializer_class = VehicleSerializer
        return super(VehicleViewSet, self).create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(VehicleViewSet, self).destroy(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        # this is the main app list view, shoud only need to be logged into view this
        self.queryset = Vehicle.objects.all().order_by('category',)
        return super(VehicleViewSet, self).list(request, *args, **kwargs)

    @list_route(methods=['get'])
    def creation_list(self,  request, *args, **kwargs):
        # this is the creation app list view, non staff/admin should only see theirs
        if request.user.is_superuser or request.user.is_staff:
            self.queryset = Vehicle.objects.all()
        else:
            self.queryset = Vehicle.objects.filter(created_by_id=request.user.id)
        return super(VehicleViewSet, self).list(request, *args, **kwargs)


class VehicleCategoryFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Category
        fields = ['title', 'deleted']


class VehicleCategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = VehicleCategorySerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, SearchFilter)
    filter_class = VehicleCategoryFilter
    search_fields = ('title','description')

    def update(self, request, *args, **kwargs):
        if request.user.is_superuser or request.user.is_staff:
            return super(VehicleCategoryViewSet, self).update(request, *args, **kwargs)

        raise MethodNotAllowed('PUT')

    def create(self, request, *args, **kwargs):
        if request.user.is_superuser or request.user.is_staff:
            return super(VehicleCategoryViewSet, self).create(request, *args, **kwargs)

        raise MethodNotAllowed('POST')

    def destroy(self, request, *args, **kwargs):
        if request.user.is_superuser or request.user.is_staff:
            return super(VehicleCategoryViewSet, self).destroy(request, *args, **kwargs)

        raise MethodNotAllowed('DELETE')


