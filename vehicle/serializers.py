from rest_framework import serializers

from vehicle.models import Vehicle, Category


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = '__all__'


class VehicleCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

