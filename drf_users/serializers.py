from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm
from django.conf import settings
from django.utils.http import urlsafe_base64_decode as uid_decoder
from django.utils.encoding import force_text
from rest_framework import serializers, exceptions
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.tokens import default_token_generator


#
# try:
#     from django.contrib.sites.models import get_current_site
# except ImportError:
#     from django.contrib.sites.shortcuts import get_current_site

from rest_framework.exceptions import ValidationError
from .models import TokenModel
from drf_users.models import User

UserModel = get_user_model()

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'full_name',
            'username',
            'first_name',
            'last_name',
            'email',
            'mobile_number',
            'date_joined',
            'is_admin',
            'is_superuser',
            'profile_pic',
            'ID_photo',
            'KRA_PIN_photo',
            'DL_Photo',

        )

    is_admin = serializers.BooleanField(read_only=True)
    is_superuser = serializers.BooleanField(read_only=True)

class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'full_name',
            'username',
            'first_name',
            'last_name',
            'email',
            'mobile_number',
            'date_joined',
            'is_admin',
            'is_superuser',
            'profile_pic',
            'ID_photo',
            'KRA_PIN_photo',
            'DL_Photo',

        )

    password = serializers.CharField(max_length=255, required=True)


class TokenSerializer(serializers.ModelSerializer):
    """
    Serializer for Token model.
    """

    class Meta:
        model = TokenModel
        fields = ('key',)

class JWTSerializer(serializers.Serializer):
    """
    Serializer for JWT authentication.
    """
    token = serializers.CharField()
    user = UserSerializer()

